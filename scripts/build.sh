#!/bin/bash
set -e

case $1 in
"release")
cmake -DCMAKE_BUILD_TYPE=Release ..;
make;
./ecs;
;;
"test")
cmake -DCMAKE_BUILD_TYPE=Debug ..;
make;
./ecs -test;
;;
"clean")
rm -rf build/Makefile build/ecs;
rm -rf vgcore.*
;;
*)
./ecs;
;;
esac
