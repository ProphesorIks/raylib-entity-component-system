#ifndef RUNNER_H
#define RUNNER_H
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sut.h"
#include "../core/ecs.h"
#include "../core/ecsinternal.h"
int runTests(void);
#endif
