#include "ecs.h"

EntityID EntityCreateID(EntityIndex entityIndex, EntityVersion entityVersion)
{
    return ((EntityID) entityIndex << (sizeof(EntityIndex) * 8)) | ((EntityID) entityVersion);
}

EntityIndex EntityGetIndex(EntityID entityID)
{
    return entityID >> (sizeof(EntityIndex) * 8);
}

EntityVersion EntityGetVersion(EntityID entityID)
{
    return (EntityVersion) entityID;
}

bool EntityIsValid(EntityID entityID)
{
    return (entityID >> (sizeof(EntityIndex) * 8)) != (EntityIndex) -1;
}
