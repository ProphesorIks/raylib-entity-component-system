#ifndef ECS_H 
#define ECS_H 

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <stdarg.h>

typedef u_int64_t VectorvDimension; // Integer size used to track Vectorv length
#define VECTORV_DIMENSION_MAX ((VectorvDimension) -1)
// Vectorv
typedef struct
{
    VectorvDimension length;
    VectorvDimension lengthAllocation; 
    size_t elementSize;
    void (*disposeFunc)(void * element);
    void * elements;
} Vectorv;

void VectorvCreate(Vectorv * vectorv, size_t elementSize, void (*disposeFunc)(void * element));
void VectorvDestroy(Vectorv * vectorv);
void VectorvPush(Vectorv * vectorv, void * dataPtr); // Copies data form provided adress
bool VectorvPop(Vectorv * vectorv, void * valueRef); // Copies data to provided adress
void * VectorvAt(Vectorv * vectorv, VectorvDimension index); // Returns pointer to data or null if not present
void * VectorvPeek(Vectorv * vectorv); // Returns adress to last element
void VectorvSet(Vectorv * vectorv, VectorvDimension index, void * element); // Sets element at index, extends allocation if needed. If allocation is extended, elements between last and last one are left at garbage values

typedef u_int64_t EntityID;
typedef u_int32_t EntityVersion;
typedef u_int32_t EntityIndex;

// generic ID type for components and systems
typedef unsigned ID;
#define ID_INVALID ((ID) -1)

//Entities
EntityID EntityCreateID(EntityIndex entityIndex, EntityVersion entityVersion);
EntityIndex EntityGetIndex(EntityID entityID);
EntityVersion EntityGetVersion(EntityID entityID);
bool EntityIsValid(EntityID entityID);

#define ENTITY_INVALID_INDEX ((EntityIndex) -1)
#define ENTITIES_MAX ((EntityIndex) -1) 

// Structure definitions 

// Events types for trigerring events 
typedef enum {
    ENUM_EVENT_TYPE_DEFAULT, //REQUIRED
    ENUM_EVENT_TYPE_START,
    ENUM_EVENT_TYPE_STOP,
    ENUM_EVENT_TYPE_LENGTH // Do not remove. Automatic variable to keep count of event types 
} EventType;

typedef struct system {
    bool isActive;
    bool isDisposed;
    Vectorv trackedComponentsIDs;
    void * (*onEventFunc)(EventType eventType, EntityID entity, Vectorv * eventData);
} System;

// Ecs header
typedef struct ecsHeader {
    Vectorv components;
    Vectorv systems;
    Vectorv entities;
    Vectorv entitiesRecycle;
} EcsHeader;

void EcsCreate(EcsHeader * ecs);
void EcsDestroy(EcsHeader * ecs);
ID EcsComponentCreate(EcsHeader * ecs , size_t componentSize, void * defaultComponentData);
void EcsComponentActiveSet(EcsHeader * ecs, ID componentID, bool active);
void EcsComponentDestroy(EcsHeader * ecs, ID componentID);
void EcsComponentGet(EcsHeader * ecs, ID componentID, EntityID entityID, void ** valueRef);
EntityID EcsEntityNext(EcsHeader * ecs);

void EcsEntityDeactivate(EcsHeader * ecs, EntityID entityID);
bool EcsEntityIsActive(EcsHeader * ecs, EntityID entityID);
void EcsEntityAttach(EcsHeader * ecs, ID componentID, EntityID entityID);
void EcsEntityDetach(EcsHeader * ecs, ID componentID, EntityID entityID);
bool EcsEntityIsAttached(EcsHeader * ecs, ID componentID, EntityID entityID);
ID EcsSystemCreate
(
    EcsHeader * ecs,
    void *(*onEventFunc)(EventType eventType, EntityID entity, Vectorv * eventData),
    const char * componentFormat,
    int componetCount,
    ...
);
void EcsSystemDestroy(EcsHeader * ecs, ID systemID);
void EcsSystemActiveSet(EcsHeader * ecs, ID systemID, bool active);
void EcsLoop(EcsHeader * ecs); // Calls onEventFunc for all attached active systems with EventType.DEFAULT
void EcsSystemLoop(EcsHeader * ecs, ID sytemID, EventType eventType);

// COMPONENTS
typedef struct
{
    float x;
    float y;
    float z;
} CmpPosition;

typedef struct
{
    int r; 
    int g;
    int b;
    int a;
} CmpColor;

typedef struct
{
    char tag;
} CmpTag;

#endif

/* Just a main header file that should be included to use ECS. Defines all exposed structs and functions.
 */
