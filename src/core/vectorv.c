#include "ecs.h"
#include "ecsinternal.h"

static inline void VectorvExtend(Vectorv * vectorv);

void VectorvCreate(Vectorv * vectorv, size_t elementSize, void (*onDispose)(void * element))
{
    assert(vectorv);

    vectorv->length = 0;
    vectorv->lengthAllocation = 0;
    vectorv->elementSize = elementSize;
    vectorv->elements = NULL;
    vectorv->disposeFunc = onDispose;
}

void VectorvDestroy(Vectorv * vectorv)
{
    assert(vectorv);
    assert(vectorv->disposeFunc);

    for(VectorvDimension i = 0; i < vectorv->length; ++i)
    {
        void * elem = VectorvNthElement(vectorv, i);
        vectorv->disposeFunc(elem);
    }

    free(vectorv->elements);
    vectorv->elements = NULL;
    vectorv->length = 0;
    vectorv->elementSize = 0;
}

void VectorvPush(Vectorv * vectorv, void * element)
{
    assert(vectorv->length < VECTORV_DIMENSION_MAX);

    if(vectorv->length >= vectorv->lengthAllocation)
    {
        VectorvExtend(vectorv);
    }

    VectorvDimension index = vectorv->length++;
    memcpy(VectorvNthElement(vectorv, index), element, vectorv->elementSize);
}

bool VectorvPop(Vectorv * vectorv, void * valueRef)
{
    assert(vectorv);
    if(vectorv->length > 0)
    {
        vectorv->length--;
        assert(valueRef);

        void * elementRef = VectorvNthElement(vectorv, vectorv->length);
        memcpy(valueRef, elementRef, vectorv->elementSize);
        return true;
    }
    return false;
}

void * VectorvAt(Vectorv * vectorv, VectorvDimension index)
{
    assert(vectorv);
    assert(index < vectorv->length);

    return VectorvNthElement(vectorv, index);
}

void * VectorvPeek(Vectorv * vectorv)
{
    assert(vectorv);
    assert(vectorv->length > 0);

    return VectorvNthElement(vectorv, vectorv->length-1);
}


void VectorvSet(Vectorv * vectorv, VectorvDimension index, void * ref)
{
    assert(vectorv);
    assert(ref);

    while(index >= vectorv->lengthAllocation)
    {
        VectorvExtend(vectorv);
    }

    if(vectorv->length <= index)
    {
        vectorv->length = index+1;
    }

    memcpy(VectorvNthElement(vectorv, index), ref, vectorv->elementSize);
}

static inline void VectorvExtend(Vectorv * vectorv)
{
    assert(vectorv->lengthAllocation < UINT64_MAX);

    vectorv->lengthAllocation *= 2;
    vectorv->lengthAllocation %= UINT64_MAX;
    vectorv->lengthAllocation =
        (vectorv->lengthAllocation > 0) ? vectorv->lengthAllocation : 1; 

    vectorv->elements = realloc(vectorv->elements, vectorv->elementSize * vectorv->lengthAllocation);
}
