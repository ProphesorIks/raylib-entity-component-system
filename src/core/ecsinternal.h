#ifndef ECSINTERNAL_H
#define ECSINTERNAL_H
#include "ecs.h"

// quick pointer math  
#define VectorvNthElement(vector,index) (((vector)->elements) + ((index) * ((vector)->elementSize)))
typedef struct
{
    bool isActive;
    bool isDisposed;
    size_t componentSize;
    void * defaultComponentData;
    Vectorv componentData;
    Vectorv mapKeys; // Reference to which entityIndex is held at dense index
    Vectorv mapSparse;
} Component;

void * ComponentGet(Component * component, EntityIndex entityIndex);

#endif

/*
    This is internal header file provided for core ECS implementation files to share common functions and defines. Outside ECS users should not import this header, and should only make use of ecs.h
 */
