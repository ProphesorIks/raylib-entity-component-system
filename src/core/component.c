#include "ecs.h"
#include "ecsinternal.h"

ID EcsComponentCreate(EcsHeader * ecs, size_t componentSize, void * defaultComponentData)
{
    assert(ecs);
    assert(defaultComponentData);

    Component component;
    component.isActive= true;
    component.isDisposed = false;
    component.componentSize = componentSize;
    component.defaultComponentData = malloc(componentSize);
    assert(component.defaultComponentData);

     // this data is used when attaching entities, so that they have default data
     memcpy(component.defaultComponentData, defaultComponentData, component.componentSize);
     VectorvCreate(&component.componentData, componentSize, NULL); 
     VectorvCreate(&component.mapKeys, sizeof(EntityIndex), NULL);
     VectorvCreate(&component.mapSparse, sizeof(EntityIndex), NULL);
        
     VectorvPush(&ecs->components, &component);
     return ecs->components.length - 1;
}

/**
 * Adds entry to sparse map, update key map and copy defaultComponentData
 */
void EcsEntityAttach(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    assert(ecs);
    assert(componentID != ID_INVALID);
    assert(ecs->components.length > componentID);

    Component * component = VectorvNthElement(&ecs->components, componentID);
    EntityIndex entityIndex = EntityGetIndex(entityID);

    assert(!component->isDisposed);
    assert(entityIndex != ENTITY_INVALID_INDEX);

    VectorvPush(&component->mapKeys, &entityIndex);
    VectorvPush(&component->componentData, component->defaultComponentData);

    // If attached entitie's id is higher than any that were attached before, we need to populate vector with invalid ids.
    while(component->mapSparse.length <= entityIndex) 
    {
        EntityIndex entityIndexInvalid = ENTITY_INVALID_INDEX;
        VectorvPush(&component->mapSparse, &entityIndexInvalid);
    }

    EntityIndex keyIndex = component->mapKeys.length - 1;  
    VectorvSet(&component->mapSparse, entityIndex, &keyIndex);
}

/*
 * Sets id on sparse map to invalid and switches entitie's data and key with last one, so that vectors always remain densly packed.
 */
void EcsEntityDetach(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    assert(EcsEntityIsAttached(ecs, componentID, entityID));

    EntityIndex entityIndex = EntityGetIndex(entityID);
    Component * component = VectorvNthElement(&ecs->components, componentID);

    assert(component);
    assert(!component->isDisposed);

    EntityIndex * sparseKeyIndexRef = VectorvNthElement(&component->mapSparse, entityIndex);

    assert(sparseKeyIndexRef);

    //swap component data
    EntityIndex sparseKeyIndex = *sparseKeyIndexRef;
    void * lastComponentData = VectorvPeek(&component->componentData);
    assert(lastComponentData != NULL);

    VectorvSet(&component->componentData, sparseKeyIndex, lastComponentData);
    EntityIndex * indexKeysLast = VectorvPeek(&component->mapKeys);
    //swap keys
    VectorvSet(&component->mapSparse, *indexKeysLast, &sparseKeyIndex);
    VectorvSet(&component->mapKeys, sparseKeyIndex, indexKeysLast);

    VectorvPop(&component->componentData, NULL);
    VectorvPop(&component->mapKeys, NULL);
    *sparseKeyIndexRef = ENTITY_INVALID_INDEX;
    
    //if we removed the last entity in sparse map, let's shrink it
    if(entityIndex == (component->mapSparse.length - 1))
    {
        EntityIndex sparseKeyIndex;
        while(VectorvPop(&component->mapSparse, &sparseKeyIndex))
        {
            if(sparseKeyIndex != ENTITY_INVALID_INDEX)
            {
                // whoops, last one poped is valid, put it back.
                VectorvPush(&component->mapSparse, &sparseKeyIndex);
                break;
            }
        }
    }
}

/*
 * Just checks if component data is present and then updated provided reference to point to said data
 */
void EcsComponentGet(EcsHeader *ecs, ID componentID, EntityID entityID, void **valueRef)
{
    assert(EcsEntityIsAttached(ecs, componentID, entityID));

    Component * component = VectorvNthElement(&ecs->components, componentID);
    void * componentData = ComponentGet(component, EntityGetIndex(entityID));
    if(componentData)
    {
        *valueRef = componentData;
    }
}

/*
 * Internal equivalent of EcsComponentGet() skips checks for if an entity is attached to ecs
 */
void * ComponentGet(Component * component, EntityIndex entityIndex)
{
    assert(!component->isDisposed && component->mapSparse.length > entityIndex);

    EntityIndex sparseKeyIndex = *((EntityIndex *) VectorvNthElement(&component->mapSparse, entityIndex));

    assert(sparseKeyIndex != ENTITY_INVALID_INDEX);
    return VectorvNthElement(&component->componentData, sparseKeyIndex);
}
/*
 * Checks if entity is attached, also if component is active (may be changed in the future).
 */
bool EcsEntityIsAttached(EcsHeader * ecs, ID componentID, EntityID entityID)
{
    assert(EcsEntityIsActive(ecs, entityID));

    EntityIndex entityIndex = EntityGetIndex(entityID);
    Component * component = VectorvNthElement(&ecs->components, componentID);

    assert(!component->isDisposed);
    assert(component->mapSparse.length > entityIndex);
    return *((EntityIndex *) VectorvNthElement(&component->mapSparse, entityIndex)) != ENTITY_INVALID_INDEX;
}

void EcsComponentDestroy(EcsHeader * ecs, ID componentID)
{
    assert(ecs);
    assert(componentID != ID_INVALID);

    Component * component = VectorvAt(&ecs->components, componentID);
    assert(component);

    ecs->components.disposeFunc(component);
    component->isDisposed = true;
}

void EcsComponentActiveSet(EcsHeader * ecs, ID componentID, bool isActive)
{
    assert(ecs);
    assert(componentID != ID_INVALID);

    Component * component = VectorvAt(&ecs->components, componentID);
    assert(component);

    component->isActive = isActive;
}
