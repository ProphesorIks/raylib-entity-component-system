#include "raylib.h"
#include "core/ecs.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#define GAME_TITLE "Game"

// Helper functions
void EcsInit();
void * Draw(EventType, EntityID, Vectorv *);
void * Move(EventType, EntityID, Vectorv *);
void CreateEntities();
void DestoryEntities();

const int screenWidth = 800;
const int screenHeight = 450;

const int textureSize = 32;
float speedMove = 0.4;
const int entitiesToCreate = 200;
unsigned long entitiesTotal = 0;
unsigned long frameCounter = 0;

// Ecs related globals
EcsHeader ecs;
ID sysDraw;
ID sysMove;
ID cmpIdPosition = ID_INVALID;
ID cmpIdColor = ID_INVALID;
EntityID e0;

Texture2D logo;

int main(int argc, char **argv)
{
    // If -test is passed, enter testing mode and return after tests are complete
    if(argc > 1 && strcmp(argv[1], "-test") == 0)
    {
        #include "tests/runner.h"
        return runTests();
    }

    const int targetFps = 120;

    InitWindow(screenWidth, screenHeight, GAME_TITLE);

    // Load example texture
    logo = LoadTexture("logo32x32.png");
    SetTargetFPS(targetFps);

    EcsInit(); // Helper function to setup example ECS

    while(!WindowShouldClose())
    {
        BeginDrawing();
            ClearBackground(GREEN);

            if(IsMouseButtonDown(MOUSE_LEFT_BUTTON))
            {
                CreateEntities();
            }

            if(IsMouseButtonDown(MOUSE_RIGHT_BUTTON))
            {
                DestoryEntities();
            }

            if(IsKeyReleased(KEY_R))
            {
                EcsDestroy(&ecs);
                EcsInit();
            }

            if(IsKeyReleased(KEY_M))
            {
                EcsSystemActiveSet(&ecs, sysMove, !((System *) VectorvAt(&ecs.systems,sysMove))->isActive);
            }

            if(IsKeyReleased(KEY_D))
            {
                EcsSystemActiveSet(&ecs, sysDraw, !((System *) VectorvAt(&ecs.systems,sysDraw))->isActive);
            }

            speedMove += GetMouseWheelMove() / 4.0;

            EcsLoop(&ecs); // MAIN ECS LOOPING CALL

            // we can also refer to entities by their id:
            CmpPosition * e0pos;
            if(EcsComponentGet(&ecs, cmpIdPosition, e0, (void **) &e0pos))
            {
                // our entity will follow mouse
                e0pos->x = GetMouseX() - textureSize/2.0;
                e0pos->y = GetMouseY() - textureSize/2.0;
            }

            Color * e0col;
            if(EcsComponentGet(&ecs, cmpIdColor, e0, (void **) &e0col))
            {
                // our entity will also shift trough colors...
                e0col->a = 250;
                e0col->r = frameCounter % 255;
                e0col->g = frameCounter + 150 % 255;
                e0col->b = frameCounter + 21 % 255;
            }


            // draw some info onto the screen
            DrawRectangle(0,0,256, screenHeight, (Color){15,15,15,220});
            char str[1024];
            sprintf(str, "total entities: %lu\nM1 - add\nM2 - remove\nMW - speed\nR - reset\nD - toggle draw\nM - toggle move", entitiesTotal);
            DrawText(str, 8, 32, 20, RED);
            DrawFPS(10,10);
        EndDrawing();
        frameCounter++; // keep track of frames
    }

    EcsDestroy(&ecs);
    CloseWindow();
    return 0;
}


void EcsInit()
{ 
    // time for randomization
    time_t t;
    time(&t);
    srand((unsigned) t);

    EcsCreate(&ecs); // VERY IMPORTANT. initializes ECS
    CmpPosition cmpDefaultPosition = {0.0,0.0, 0.0}; // You can use custom components...
    Color cmpDefaultColor = {255,155,55,255}; // ...or any kind of structures
   
    // create components
    cmpIdPosition = EcsComponentCreate(&ecs, sizeof(CmpPosition), &cmpDefaultPosition);
    cmpIdColor = EcsComponentCreate(&ecs, sizeof(Color), &cmpDefaultColor);
       
    // create systems
    sysMove = EcsSystemCreate(&ecs, Move, "a", 1, cmpIdPosition);
    sysDraw = EcsSystemCreate(&ecs, Draw, "aa", 2, cmpIdPosition, cmpIdColor);
   
    // let's create some entities too
    entitiesTotal = 0;
    CreateEntities();
}

void CreateEntities()
{
    for(int i = 0; i < entitiesToCreate; i++)
    {
        EntityID entity = EcsEntityNext(&ecs);
        if(i == entitiesToCreate -1)
        {
            e0 = entity;
        }
        EcsEntityAttach(&ecs, cmpIdPosition, entity);
        EcsEntityAttach(&ecs, cmpIdColor, entity);

        CmpPosition * position;
        EcsComponentGet(&ecs, cmpIdPosition, entity, (void **) &position);
        Color * color; 
        EcsComponentGet(&ecs, cmpIdColor, entity, (void **) &color);

        position->x = rand() % screenWidth-8 + 4;
        position->y = rand() % screenHeight-8 + 4;
        position->z = rand() % 360;

        color->r = rand() % 255;
        color->g = rand() % 255;
        color->b = rand() % 255;
        color->a = rand() % 255;

        entitiesTotal++;
    }
}

void DestoryEntities()
{
    int entitiesDestroyed = 0;
    for(int i = 0; i < ecs.entities.length; i++)
    {
        EntityID *ref = VectorvAt(&ecs.entities, i);

        if(ref)
        {
            EntityID entity = *ref;
            if(EntityIsValid(entity))
            {
                if
                (
                    EcsEntityDeactivate(&ecs, entity) 
                )
                {
                    entitiesDestroyed++; 
                    entitiesTotal--;
                }
                if(entitiesDestroyed >= entitiesToCreate)
                {
                    return;
                }
            }
        }
    }
}

void * Move(EventType event, EntityID entity, Vectorv * data)
{
    // this is the real magic. we have a position component with x y and z
    // Since our demo is 2d, we use z for direction
    CmpPosition * position = *((CmpPosition **) VectorvAt(data, 0));
    position->x += cos(DEG2RAD * position->z) * speedMove;
    position->y += sin(DEG2RAD * position->z) * speedMove;

    // Upon colision, let's bounce off. 
    if((int) position->x > (screenWidth - textureSize/2) ||(int) position->x < textureSize/2|| (int) position->y > (screenHeight - textureSize/2) ||(int) position->y < textureSize/2)
    {
        position->z = ((position->z + 90));
    }

    return NULL;
}


void * Draw(EventType event, EntityID entity, Vectorv * data)
{
    // Draw system is attached to 2 components, first one is position and the second one is color
    CmpPosition * position = *((CmpPosition **) VectorvAt(data, 0));
    Color * color = *((Color **) VectorvAt(data, 1));
    DrawTexture(logo, position->x, position->y, *color);

    return NULL;
}
