<h4>Simple ECS</h4>

Hello,
Welcome to simple ECS (entity-component-system) written in **c99** with no external dependencies (depends on [raylib](www.raylib.com) for showcase)
Main goals and features:

- No external dependencies
- c99
- Minimal testing framework
- Easy to setup and use
- Minimal 
- _Wide_ coding style
- TDD
- performance oriented

---

Blah blah nobody cares about that. Let's jump into setup.

I advise you to checkout [**wiki**](https://gitlab.com/ProphesorIks/raylib-entity-component-system/-/wikis/home) and [**quickstart guide**](https://gitlab.com/ProphesorIks/raylib-entity-component-system/-/wikis/wiki/quickstart)

**note**: build configurations were only tested on linux. You may experience some issues with makefile on other platforms. Also in src/tests/runner.c and src/tests/sut.h linux specific character escape characters are used for coloured text in printf's. You might want to remove them if text is displayed incorrectly.

<h4>Setup</h4>

clone this repo, enter it and run `build.sh` (if you are on linux)
otherwise run `make`. By default it depends on raylib for showcase, but you can pass `NO_RAYLIB=TRUE` and of course remove all calls to raylib functions (`make NO_RAYLIB=TRUE`). Then run `debug/game` executable.

<h4>Testing</h4>

Pass `-test` to executable or run `build.sh test` to enter testing mode. Testing uses simple assertions, with a bit of fanciness to count how many were made and print which one failed. `master` branch always has 100% coverage and non 100% coverege is to be assumed a mistake in `dev`

<h4>Author and contributing</h4>

This is my side project which I intend to use for game development with raylib. Written from scratch, it's my first _real_ open source project, so any contributors are really welcome. Feel free to roast me and point out every my single mistake, it's hard to offend me and I really want to learn from the toughest of c programmers with _no bullshit attitude_. All code you contribute must have it's own unit tests and/or not fail current ones. But even if you are just a regular coder with eagerness to learn, who had never previously contributed not even one commit to open source projects, don't hesitate and fork, merge, comment, submit issues, etc. etc.
Hope this little piece of software proves itself useful and you have fun time with it!
